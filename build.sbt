name := "SideEffectConvenience"

version := "0.1.1"

scalaVersion := "2.12.10"

organization := "com.gitlab.linde9821"

libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)