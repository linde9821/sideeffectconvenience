package gitlab.com.linde9821.sideeffectconvenience

object Sec {
  def printResult[A](codeBlock: => A): Unit = {
    println(codeBlock)
  }

  def printAndGetResult[A](codeBlock: => A): A = {
    val result = codeBlock
    println(result)
    result
  }

  def getResultAndTime[A](codeBlock: => A)(timeConverter: Double = 1000000d): (Double, A) = {
    val startTime = System.nanoTime
    val result = codeBlock
    ((System.nanoTime - startTime) / timeConverter, result)
  }
}
