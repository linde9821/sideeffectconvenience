package gitlab.com.linde9821.sideeffectconvenience

import org.scalatest.FunSuite

class SecTest extends FunSuite {
  test("200 integers") {

    assertResult(5050) {
      Sec.getResultAndTime{
        val list = (1 to 100).toList
        list.sum
      }()._2
    }
  }
}
